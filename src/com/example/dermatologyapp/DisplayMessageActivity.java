package com.example.dermatologyapp;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;
import android.provider.MediaStore;

import org.opencv.imgproc.Imgproc;
import org.opencv.photo.*;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.highgui.*;

public class DisplayMessageActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		dispatchTakePictureIntent();
	}
	
	static final int REQUEST_IMAGE_CAPTURE = 1;

	private void dispatchTakePictureIntent() {
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
	    }
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
	        Bundle extras = data.getExtras();
	        Bitmap imageBitmap = (Bitmap) extras.get("data");
	        
	        //CONVERTIR BITMAP A MAT
	        Mat input_image4C = new Mat(); 
	        Mat input_image = new Mat(); 
	        
	        Utils.bitmapToMat(imageBitmap, input_image4C);
	        Imgproc.cvtColor(input_image4C, input_image, Imgproc.COLOR_RGBA2RGB);
	        
	        //APLICAR EL BILATERAL FILTER
	        Mat bilateral_image = new Mat();
	        Imgproc.bilateralFilter(input_image, bilateral_image, 30, 100, 100);
	        
	        //APLICAMOS CANNY
	        Mat gray_image = new Mat();
	        Mat canny_output = new Mat();
	        
	        Imgproc.cvtColor(bilateral_image, gray_image, Imgproc.COLOR_RGB2GRAY);

	        int lowThreshold;
	    	int ratio = 3;

	    	int kTam = Math.max(input_image.cols(),input_image.rows());

	    	if(kTam < 500)
	    		lowThreshold = 60;
	    	else if(kTam < 1400)
	    		lowThreshold = 6;
	    	else if(kTam < 1800)
	    		lowThreshold = 18;
	    	else if(kTam < 2500)
	    		lowThreshold = 15;
	    	else
	    		lowThreshold = 6;

	    	Imgproc.Canny(gray_image, canny_output, lowThreshold, lowThreshold*ratio);

	    	//APLICAMOS DILATION
	    	Mat dilated_img = new Mat();
	    	Mat kernelD = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,new Size(3,3), new Point(1,1));
	    	Imgproc.dilate(canny_output, dilated_img, kernelD);
	    	
	    	//FindContours a la imagen de canny
	    	List<MatOfPoint> contours = new ArrayList<MatOfPoint>();   
	    	List<MatOfPoint> contoursSelected = new ArrayList<MatOfPoint>(); 
	    	
	    	Imgproc.findContours(dilated_img, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
	    	
	    	//Filtramos esos contornos
	    	double area;
	    	double circleArea;
	    	int percent;
	    	Point center = new Point();
	    	float[] radius = new float[1];;
	    	double perimetro;
	    	float d;
	    	int minLado = Math.min(input_image.cols(), input_image.rows());
	    	
	    	//calcular irregularidad de los bordes y el tama�o que ocupa respecto a la imagen
	    	for(int i=0;i<contours.size();i++)
	    	{
	    		area =  Imgproc.contourArea(contours.get(i));
	    		Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(i).toArray()), center, radius);
	    		
	    		//calculamos el porcentaje de �rea que ocupa el melanoma en el c�rculo
	    		circleArea = Math.PI*radius[0]*radius[0];
	    		
	    		percent = (int) ((100*area)/circleArea);
	    		
	    		perimetro = Imgproc.arcLength(new MatOfPoint2f(contours.get(i).toArray()), true );

	    		d = radius[0]*2;
	    		
	    		if(percent < 70  && percent > 20 && 100*d/minLado > 3)
	    			contoursSelected.add(contours.get(i));
	    	}
	    	
	    	Mat draw_output = new Mat();
	    	Imgproc.drawContours(draw_output, contoursSelected, -1, new Scalar(255));
	    	
	        //CONVERTIR MAT A BITMAP
	        Bitmap bm = Bitmap.createBitmap(draw_output.cols(), draw_output.rows(),Bitmap.Config.ARGB_8888);
	        Utils.matToBitmap(draw_output, bm);
	       
	        
	        //CREAR UN IMAGEVIEW PARA MOSTRAR UNA IMAGEN
	        ImageView imageView = new ImageView(getApplicationContext());
	        imageView.setImageBitmap(bm);
	        
    	    //display the imageview 
    	    setContentView(imageView);
    	    
	    }else if(resultCode == RESULT_CANCELED)
	    {
	    	finish();
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_message, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_display_message,
					container, false);
			return rootView;
		}
	}

}
